package intermediate;

public class lab_4 {
	
	public static void main(String[] args)
	{
		for(int i=2 ;i<100 ; i++)
		{
			if(primeseries(i))
			{
				System.out.print( i +" ");
			}
		}
	}

	private static boolean primeseries(int n) 
	{
		 for(int i=2 ; i<n ; i++)
		 {
			 if(n %i == 0)
			 {
				 return false ;
			 }
		 }
		
		return true;
	}

}
