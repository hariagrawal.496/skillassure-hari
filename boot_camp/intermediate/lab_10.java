package intermediate;

import java.util.Scanner;

public class lab_10 {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		System.out.println("Enter the number : ");
		 int num = sc.nextInt();
		 
		 int[] arr = new int[num];
		 
		  arr[0] = 0 ;
		  arr[1] = 1 ;
		  
		 for(int i=2 ; i<arr.length ; i++)
		 {
			 arr[i] = arr[i-2] +arr[i-1];
		 }
		 
		 for(int i=0 ; i<arr.length ; i++)
		 {
			 System.out.print(arr[i] + " ");
		 }
		 
	}

}
