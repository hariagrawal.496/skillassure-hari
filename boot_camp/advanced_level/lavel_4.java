package advanced_level;

import java.util.Scanner;

public class lavel_4 {
	
	 static  Scanner sc= new Scanner(System.in);
	public static void main(String[] args)
	{
		  System.out.println("Enter the name of employee ..");
		    String name = sc.nextLine();
		   
		  System.out.println("Enter the Id of employee ..");
			String empId = sc.nextLine(); 
		  
		  System.out.println("Enter the basic salary of employee ..");
			double basic_salary = sc.nextDouble();
			
		  System.out.println("Enter the allowance amount of employee ..");
		    double allowance_salary = sc.nextDouble();
		    
		  System.out.println("Enter the bonus percentage of employee..  ");
		    double bonus_percentage = sc.nextDouble();
		    
		  System.out.println("Enter the monthly investment of employee ..");
			double monthly_investment = sc.nextDouble();
			
		   double gross_income_monthly = basic_salary + allowance_salary ;
		   
		   double annual_salary =  gross_income_monthly *12  +
				   gross_income_monthly * (bonus_percentage /100)  ;
				   
		   double annual_investment =  monthly_investment *12 ;
		   
		  double total_annual_salary =  annual_salary + annual_investment ;
		    
		   double total_tax_amount = 0 ;
		   double annual_gross_salary = 0 ;
		   double annual_net_salary = 0 ;
		   
		  
		  if(total_annual_salary < 400000  && annual_salary < 250000)
		  {
			   total_tax_amount = 0 ;
			   annual_gross_salary = total_annual_salary ;
			   annual_net_salary = total_annual_salary ;
			   
		  }
		  else if(annual_salary < 500000 && annual_salary >=250000)
		  {
			  if(annual_investment<150000)
			  {
				  total_tax_amount = annual_salary * 5 /100  ;
			  }
			  else
			  {
				 double  remaining_investment_amount_for_tax_paid = (annual_investment -150000) ;
				 
				  total_tax_amount = annual_salary*5 /100  + 
						  remaining_investment_amount_for_tax_paid * 5/100 ;
			  }
			   
			   annual_gross_salary = total_annual_salary  ;
			   annual_net_salary = total_annual_salary -total_tax_amount ;
			   
		  }
		  else if(annual_salary < 1000000 && annual_salary >=500000)
		  {
			  if(annual_investment<150000)
			  {
				  total_tax_amount = (250000 * 5/100) + (annual_salary  -500000)*20 /100 ;
			  }
			  else
			  {
				 double  remaining_investment_amount_for_tax_paid = (annual_investment -150000) ;
				 
				  total_tax_amount = (250000 * 5/100) + (annual_salary  -500000)*20 /100  + 
						  remaining_investment_amount_for_tax_paid * 20/100 ;
			  }
			  
			   annual_gross_salary = total_annual_salary ;
			   annual_net_salary = total_annual_salary -total_tax_amount ;
		  }
		  else
		  {
			  if(annual_investment<150000)
			  {
				  total_tax_amount = (250000 * 5/100) + (500000 * 20/100) +
						             (annual_salary  -1000000)*30 /100 ;
			  }
			  else
			  {
				 double  remaining_investment_amount_for_tax_paid = (annual_investment -150000) ;
				 
				  total_tax_amount = (250000 * 5/100) + (500000 * 20/100) +
						              (annual_salary  -1000000)*30 /100  + 
						             remaining_investment_amount_for_tax_paid * 30/100 ;
			  }
			  
			   annual_gross_salary = total_annual_salary ;
			   annual_net_salary = total_annual_salary -total_tax_amount ;
		  }
		   
		  System.out.println("The employee annual gross income is : " +annual_gross_salary );
		  System.out.println("The employee annual net income is : " +annual_net_salary );
		  System.out.println("The employee annual tax payed is : " +total_tax_amount );	
			
	}

}
